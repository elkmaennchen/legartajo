#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import argparse
import csv
from PIL import Image
from PIL import ImageColor
import math
import sys

def parseCmdLine():
    parser = argparse.ArgumentParser(
        prog = "legartajo",
        description = "Legartaĵo compute Lego® representation of an image.",
        usage="%(prog)s [OPTION] FILE(S)",
        epilog = "See more at https://gitlab.crans.org/elkmaennchen/legartajo")

    parser.add_argument('-l', '--limitsize', dest='only1x1', default=False, action='store_true',
        help="Use only 1x1 flat tile.")

    parser.add_argument('-i', '--no-image', dest='generate_output_image', default=True, action='store_false',
        help="Do not generate output image.")

    parser.add_argument('-d', '--display', dest='display', default=False, action='store_true',
        help="Display images.")
    
    parser.add_argument('-s', '--size', dest='base_size', type=int, default=48,
        help='Size of the baseplate. Defaut if 48, 32 is also a popular value.')

    parser.add_argument('files', nargs='+',
        help="Image(s) to process.")
    
    return(parser.parse_args())

def fetchLegoCodes(dbcodes_filename):
    global legodb_dct
    global legocolors_dct

    with open('legodb.csv', newline='') as legocsvdb:
        reader = csv.DictReader(legocsvdb, delimiter=',')
        for row in reader:
            legodb_dct[row['id_Ldraw']] = row
            legocolors_dct[row['id_Ldraw']] = ImageColor.getcolor("#"+row['hex_color'], "RGB")

def checkImage(img, iscontinue):
    if iscontinue and img.width != img.height:
        print("La photo doit être carrée", file=sys.stderr)
        iscontinue = False

    if iscontinue and img.mode != 'RGB':
        print("La photo doit être en RGB", file=sys.stderr)
        iscontinue = False
    
    return(iscontinue)

def computeLegoCodes(img_original, img_lego, cmd_args):
    global legocolors_dct
    # TODO touver meilleur expression comme img_legocolor = [[-1] * cmd_args.base_size] * cmd_args.base_size
    img_legocolor = []
    for _ in range(cmd_args.base_size):
        img_legocolor.append([-1] * cmd_args.base_size)
    img_original_red = img_original.resize((cmd_args.base_size, cmd_args.base_size))

    for i in range(cmd_args.base_size):
        for j in range(cmd_args.base_size):
            ppetite_distance = math.inf
            for id_color in iter(legocolors_dct):
                distance = math.dist(img_original_red.getpixel((i, j)), legocolors_dct[id_color])
                if distance < ppetite_distance:
                    ppetite_distance = distance
                    best_id = id_color
            img_lego.putpixel((i, j), legocolors_dct[best_id])
            img_legocolor[i][j] = best_id

    return(img_legocolor)

def __computeLegoBricksVlxL(L, l, base_size, img_legocolor, order):
    global legodb_dct
    for i in range(base_size):
        for j in range(base_size):
            if i <= base_size-L and j <= base_size-l:
                end = i+1
                possible = True
                while possible and end < i+L :
                    for jp in range(j, j+l):
                        if img_legocolor[end][jp] != img_legocolor[i][j] or img_legocolor[i][j] == None:
                            possible = False
                    end += 1
                if possible and legodb_dct[img_legocolor[i][j]]['pp_flat_'+str(l)+'x'+str(L)] != '':
                    order[img_legocolor[i][j]][str(l)+"x"+str(L)] += 1
                    for ip in range(i, end):
                        for jp in range(j, j+l):
                            img_legocolor[ip][jp] = None

def __computeLegoBricksHlxL(L, l, base_size, img_legocolor, order):
    global legodb_dct
    for i in range(base_size):
        for j in range(base_size):
            if i <= base_size-l and j <= base_size-L:
                end = j+1
                possible = True
                while possible and end < j+L :
                    for ip in range(i, i+l):
                        if img_legocolor[ip][end] != img_legocolor[i][j] or img_legocolor[i][j] == None:
                            possible = False
                    end += 1
                #print(img_legocolor[i][j])
                if possible and img_legocolor[i][j] != None and legodb_dct[img_legocolor[i][j]]['pp_flat_'+str(l)+'x'+str(L)] != '':
                    order[img_legocolor[i][j]][str(l)+"x"+str(L)] += 1
                    for jp in range(j, end):
                        for ip in range(i, i+l):
                            img_legocolor[ip][jp] = None

def computeLegoBricks(img_legocolor, cmd_args):
    global legodb_dct
    pricebrick = 0

    if cmd_args.only1x1:
        order = {id_color: 0 for id_color in legodb_dct.keys()}
        for line in img_legocolor:
            for pixel in line:
                order[pixel] += 1

        for id_color in order.keys():
            pricebrick += order[id_color] * float(legodb_dct[id_color]['pp_flat_1x1']) / 100

    else:
        lst_brick_size = ["1x1", "1x2", "1x3", "1x4", "1x6", "1x8", "2x2", "2x3", "2x4", "1x2x2"]
        lst_brick_surface = [1, 2, 3, 4, 6, 8, 4, 6, 8, 3]
        
        # TODO : compute real price of configuration instead of prioritize cheaper bricks
        #lst_brick_surfaceprice = [float(legodb_dct['0']['pp_flat_'+lst_brick_size[size_index]])/lst_brick_surface[size_index] for size_index in range(len(lst_brick_size))]
        #print(lst_brick_surfaceprice)
        # TODO : include 1x2x2 bricks
    
        order = {id_color: {size: 0 for size in lst_brick_size} for id_color in legodb_dct.keys()}

        # try 1x8 first axe
        __computeLegoBricksVlxL(8, 1, cmd_args.base_size, img_legocolor, order)
        # try 1x8 second axe
        __computeLegoBricksHlxL(8, 1, cmd_args.base_size, img_legocolor, order)

        __computeLegoBricksVlxL(2, 2, cmd_args.base_size, img_legocolor, order)
        __computeLegoBricksHlxL(2, 2, cmd_args.base_size, img_legocolor, order)

        __computeLegoBricksVlxL(6, 1, cmd_args.base_size, img_legocolor, order)
        __computeLegoBricksHlxL(6, 1, cmd_args.base_size, img_legocolor, order)

        __computeLegoBricksVlxL(4, 1, cmd_args.base_size, img_legocolor, order)
        __computeLegoBricksHlxL(4, 1, cmd_args.base_size, img_legocolor, order)

        __computeLegoBricksVlxL(3, 1, cmd_args.base_size, img_legocolor, order)
        __computeLegoBricksHlxL(3, 1, cmd_args.base_size, img_legocolor, order)

        __computeLegoBricksVlxL(2, 1, cmd_args.base_size, img_legocolor, order)
        __computeLegoBricksHlxL(2, 1, cmd_args.base_size, img_legocolor, order)

        __computeLegoBricksHlxL(1, 1, cmd_args.base_size, img_legocolor, order)

        for id_color in order.keys():
            for size in lst_brick_size:
                if order[id_color][size]:
                    pricebrick += order[id_color][size] * float(legodb_dct[id_color]['pp_flat_'+size]) / 100

        '''Debug
        print(order)
        counter = 0
        for color_order in order.values():
            for size in range(len(lst_brick_size)):
                counter += color_order[lst_brick_size[size]] * lst_brick_surface[size]
        print("{} of {} bricks assigned.".format(counter, cmd_args.base_size**2))'''


    if cmd_args.base_size == 48:
        pricebase = 14.99
        price = pricebrick + pricebase
    elif cmd_args.base_size == 32:
        pricebase = 8.99
        price = pricebrick + pricebase
    else:
        pricebase = 'unknown'
        price = pricebrick

    print("Prix : {0:03.2f} € ({1:03.2f} € de briques, {2:03.2f} € de plaque)".format(price, pricebrick, pricebase))

    return(order)

def buyLegoBricks(order, cmd_args):
    global legodb_dct

    if cmd_args.only1x1:
        for id_color in order.keys():
            if order[id_color]:
                print("{}\thttps://www.lego.com/fr-fr/pick-and-build/pick-a-brick?query={}&page=1&perPage=400&sort.key=RELEVANCE&sort.direction=DESC&includeOutOfStock=true".format(order[id_color], legodb_dct[id_color]['pn_flat_1x1']))

    else:
        for id_color in order.keys():
            for size in order[id_color].keys():
                if order[id_color][size]:
                    print("{}\thttps://www.lego.com/fr-fr/pick-and-build/pick-a-brick?query={}&page=1&perPage=400&sort.key=RELEVANCE&sort.direction=DESC&includeOutOfStock=true".format(order[id_color][size], legodb_dct[id_color]['pn_flat_'+size]))

def processImage(image_filename, cmd_args):
    img_lego = Image.new('RGB', (cmd_args.base_size, cmd_args.base_size))
    iscontinue = True

    try:
        img_original = Image.open(image_filename)
    except:
        print("Impossible d'ouvrir la photo", file=sys.stderr)
        iscontinue = False

    iscontinue = checkImage(img_original, iscontinue)

    if iscontinue:
        img_legocolor = computeLegoCodes(img_original, img_lego, cmd_args)

        order = computeLegoBricks(img_legocolor, cmd_args)
    
        buyLegoBricks(order, cmd_args)

        if cmd_args.display:
            img_lego.show("Après convertion lego")

        if cmd_args.generate_output_image:
            image_filename_extention = image_filename.split('.')[-1]
            img_lego.save(image_filename.removesuffix(image_filename_extention)+"lego."+image_filename_extention)

args = parseCmdLine()

legodb_dct = {}
legocolors_dct = {}
fetchLegoCodes("lego_db.csv")

for filename in args.files:
    processImage(filename, args)
