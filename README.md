# Legartaĵo

## Présentation

LEGO®, ci-après nommé Lego parce que j'ai un chouilla la flemme, propose un produit, "mosaïque", qui peut paraître intéressant : [ici sur le site Lego](https://www.lego.com/fr-fr/product/mosaic-maker-40179).

Cependant, il y a quelques défauts :
 * l'utilisation de briques `plate 1x1`, moins facile à nettoyer que des `flat tile 1x1` ;
 * limitation à 5 couleurs de briques pour en faire une boîte alors que beaucoup d'autres couleurs existent ;
 * devoir téléverser les photos que l'on veut convertir en "mosaïque" sur les serveurs Lego ;
 * se limiter à la taille de plaque, ou tout autre paramètre, imposé par Lego.

Legartaĵo est un script qui propose de convertir vos images en mosaïque, tout aussi bien que le site Lego tout en résolvant les problèmes évoqués ci-dessus.

Il propose également de ne pas se restreindre aux briques `1x1` pour réduire le coût de la construction.

## Utilisation

Dans le dossier du script, vous pouvez obtenir le mode d'emploi complet en tapant :

```bash
python3 legartajo.py -h
```

Quelques exemples d'utilisation commun :

```bash
## Convetir les images photo1.jpg et photo2.png
python3 legartajo.py photo1.jpg photo2.png

## Convetir toutes les images jpeg du dossier /home/pseudo/Images/toLego
python3 legartajo.py /home/pseudo/Images/toLego/*.jpg

## Convetir l'image photo1.jpg, sur une plaque de taille 32x32, sans générer sur le disque l'image de sortie, en affichant le rendu de l'image de sortie
python3 legartajo.py -s 32 -id photo1.jpg

## Convetir l'image photo1.jpg en utilisant seulement des briques 1x1
python3 legartajo.py -l photo1.jpg
```

À propos de l'option `-l, --limitsize`, les réalisations sont généralement plus chères en utilisant que des briques `1x1`.
Pour donner un ordre d'idée, car cela dépend fortement de la photo et de la disponibilité/prix des briques (voir section suivante), sur une image de test le prix passait de 153.23 € avec `-l` (plus que le set Lego de mosaïque) à 74.24 € sans (moins que le set Lego).

## Données

Pour fonctionner, ce script utilise une base de donnée (fournie avec le programme sous la même licence) contenant l'existance et le prix de chaque brique "flat" pour toutes les couleurs connues à un instant t.

Lego modifie cependant régulièrement son catalogue, certaines données vont donc périmer.
Si vous remarquez qu'une information n'est pas à jour, vous trouverez dans la section suivante des contacts pour signaler et remédier à la situation.

## Contact

Pour toute idée d'amélioration, signalement de bug, question sur l'utilisation, vous pouvez utiliser les outils de communication de l'instance gitlab du Crans ou envoyer un mail à l'adresse <elkmaennchen+legartajo@crans.org>.
